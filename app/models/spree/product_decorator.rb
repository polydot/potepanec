module Spree::ProductDecorator
  def related_products(number)
    Spree::Product.in_taxons(taxons).includes(master: [:default_price, :images]).
      where.not(id: id).distinct.limit(number)
  end

  Spree::Product.prepend self
end
