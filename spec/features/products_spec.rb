require 'rails_helper'

RSpec.feature "Products", type: :feature do
  describe "display product details" do
    given(:taxonomy) { create(:taxonomy, name: "Category") }
    given(:taxon) do
      create(:taxon, name: "Taxon", taxonomy: taxonomy, parent: taxonomy.root)
    end
    given(:other_taxon) do
      create(:taxon, name: "other_taxon", taxonomy: taxonomy, parent: taxonomy.root)
    end
    given(:product) { create(:product, name: "Product", taxons: [taxon]) }
    given(:other_product) do
      create(:product, name: "other_product", price: "10.10", taxons: [other_taxon])
    end
    given!(:related_products) do
      4.times.collect do |i|
        create(:product, name: "related_product#{i + 1}",
                         price: "#{rand(1.0..99.9).round(2)}",
                         taxons: [taxon])
      end
    end

    background { visit potepan_product_path(product.id) }

    scenario "display product information" do
      expect(page).to have_title "#{product.name} - BIGBAG Store"
      expect(page).to have_selector '.page-title h2', text: product.name
      expect(page).to have_selector '.col-xs-6 li', text: product.name
      expect(page).to have_selector '.media-body h2', text: product.name
      expect(page).to have_selector 'h3', text: product.display_price
      expect(page).to have_content product.description
      expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(product.taxons.first.id)
    end

    scenario "go to top page" do
      click_on 'Home', match: :first
      expect(page).to have_current_path potepan_path
    end

    scenario "display related products correctly" do
      visit potepan_product_path(product.id)
      expect(page).not_to have_selector ".productCaption h5", text: product.name
      expect(page).not_to have_selector ".productCaption h3", text: product.display_price
      expect(page).not_to have_selector ".productCaption h5", text: other_product.name
      expect(page).not_to have_selector ".productCaption h3", text: other_product.display_price
      related_products.each do |related_product|
        expect(page).to have_selector ".productCaption h5", text: related_product.name
        expect(page).to have_selector ".productCaption h3", text: related_product.display_price
      end
    end
  end
end
