require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'def full_title' do
    context 'If the argument is a string' do
      it 'display arguments in title' do
        expect(full_title('test')).to eq 'test - BIGBAG Store'
      end
    end

    context 'If the argument is empty string' do
      it 'display as BIGBAG Store' do
        expect(full_title('')).to eq 'BIGBAG Store'
      end
    end

    context 'If the argument is nil' do
      it 'display as BIGBAG Store' do
        expect(full_title(nil)).to eq 'BIGBAG Store'
      end
    end
  end
end
