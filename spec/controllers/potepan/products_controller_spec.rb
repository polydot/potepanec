require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let(:category) { create(:taxonomy, name: "Category") }
    let(:taxon) { create(:taxon, name: "Taxon", taxonomy: category, parent: category.root) }
    let(:other_taxon) do
      create(:taxon, name: "Other_taxon", taxonomy: category, parent: category.root)
    end
    let(:product) { create :product }
    let(:other_product) do
      create(:product, name: "other_product", price: "98.76", taxons: [other_taxon])
    end
    let!(:related_products) do
      create_list(:product, 4, name: "related_product", price: "10.10", taxons: [taxon])
    end

    before do
      get :show, params: { id: product.id }
    end

    it "responds successfully" do
      expect(response).to have_http_status "200"
    end

    it "assigns @product" do
      expect(assigns(:product)).to eq product
    end

    it "render show page" do
      expect(response).to render_template "potepan/products/show"
    end

    it "assigns related_products" do
      expect(assigns(:related_products)).to match_array related_products
      expect(assigns(:related_products)).not_to include product
      expect(assigns(:related_products)).not_to include other_product
    end
  end
end
