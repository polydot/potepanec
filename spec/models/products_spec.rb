require 'rails_helper'

RSpec.describe "Product_model", type: :model do
  let(:category) { create(:taxonomy, name: "Category") }
  let(:taxon) { create(:taxon, name: "Taxon", taxonomy: category, parent: category.root) }
  let(:product) { create(:product, taxons: [taxon], name: "Product") }
  let(:related_products_limit) do
    product.related_products(LIMIT_OF_RELATED_PRODUCTS)
  end

  describe "related_products" do
    context "There are 3 related products" do
      let!(:related_products) do
        create_list(:product, 3, taxons: [taxon])
      end

      it "Will only get 3 products" do
        stub_const("LIMIT_OF_RELATED_PRODUCTS", 4)
        expect(related_products_limit.size).to eq 3
      end
    end

    context "There are 4 related products" do
      let!(:related_products) do
        create_list(:product, 4, taxons: [taxon])
      end

      it "Will get 4 products" do
        stub_const("LIMIT_OF_RELATED_PRODUCTS", 4)
        expect(related_products_limit.size).to eq 4
      end
    end

    context "There are 5 related products" do
      let!(:related_products) do
        create_list(:product, 5, taxons: [taxon])
      end

      it "If you have 5 related products you get 4 products" do
        stub_const("LIMIT_OF_RELATED_PRODUCTS", 4)
        expect(related_products_limit.size).to eq 4
      end
    end
  end
end
